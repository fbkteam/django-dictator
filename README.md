# Dictator

Block content editor

## Dependencies
- Django
- django-appconf
- Pillow
- sorl-thumbnail

## Installation

```
$ git clone git@bitbucket.org:fbkteam/django-dictator.git
```
or add this line to the pip requirements file:
```
-e git+git@bitbucket.org:fbkteam/django-dictator.git#egg=django-dictator
```

## Setup

Add 'dictator' to INSTALLED_APPS after all of your apps.
```
INSTALLED_APPS += ( 'django_markdown', )
```

Add dictator urls to base urls
```
import dictator.urls
urlpatterns += [
    url(r'^dictator/', include(dictator.urls)),
]
```

##Use dictator

###  Models

```python
from dictator.fields import DictatorField
content = DictatorField(default='', null=False, blank=True, dictator_conf={})
```

### Templates
```html
<div>{{ post.content.html|safe }}</div>
```

## Settings

По умолчанию для рендеринга блоков используются дефолтные шаблоны из `dictator/templates/dictator/blocks`

Их можно переопределить, добавив соответсвующие файлы в шаблоны проекта в `.../templates/dictator/blocks`:

К примеру, для переопределения шаблона блока "image" нужно добавить файл `django_app/templates/dictator/blocks/image.html`:

```
    django_app/templates:
        - ...
        - dictator:
            - blocks:
                - image.html
```

_в дефолтных шаблонах ожидаются данные в соответсвующих полях (к примеру в шаблоне image.html изображение должно быть в поле file). Если поля моделей называются иначе, то нужно переопределить шаблон и указать в нем соотвесвующие названия._
