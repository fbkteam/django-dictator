from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from appconf import AppConf


class DictatorConf(AppConf):
    BLOCK_TYPES = ['cut', 'embedly', 'gallery', 'heading', 'image', 'lead', 'list', 'quote', 'text', 'video']
    REQUIRED = []
    UPLOAD_URL = None
    UPLOAD_PATH = 'attachments'
    ATTACHMENT_PROCESSOR = None

    def configure_upload_url(self, value):
        if value is not None:
            return value
        return reverse_lazy('dictator_attachments')
