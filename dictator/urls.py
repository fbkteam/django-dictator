from django.conf.urls import url
from .views import attachment


urlpatterns = [
    url('^attachments/', attachment, name='dictator_attachments'),
]
