from django import forms
from .widgets import DictatorWidget


class DictatorFormField(forms.CharField):
    def __init__(self, *args, **kwargs):
        self.widget = DictatorWidget(**kwargs.pop('dictator_conf', {}))
        super(DictatorFormField, self).__init__(*args, **kwargs)


class AttachmentForm(forms.Form):
    attachment = forms.FileField()
