# coding: utf-8
#
# import markdown2
import re
from django import template
from django.template.defaultfilters import stringfilter
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe


register = template.Library()


class WebView(object):

    strike = re.compile(r'~~(.*?)~~', re.DOTALL)
    bold = re.compile(r'\*\*(.*?)\*\*', re.DOTALL)
    italic = re.compile(r'_(.*?[^\\])_', re.DOTALL)
    list = re.compile(r'^ - (.*?)(\n|$)', re.DOTALL | re.MULTILINE)
    nl = re.compile(r'\n', re.DOTALL)
    hint = re.compile(r'\[([^\]]+)\]\{(.+?(?<!\\))\}', re.DOTALL)
    link = re.compile(r'\[([^\]]+)\]\((.+?(?<!\\))\)', re.DOTALL)  # (?<!pattern) — http://habrahabr.ru/post/159483/

    # список экранирования из sir-travor.js
    # html = html.replace(/\\\*/g, "*")
    #            .replace(/\\\[/g, "[")
    #            .replace(/\\\]/g, "]")
    #            .replace(/\\_/g, "_")
    #            .replace(/\\\(/g, "(")
    #            .replace(/\\\)/g, ")")
    #            .replace(/\\\-/g, "-")
    #            .replace(/\\~/g, "~");
    unescape = re.compile(r'\\([\*\[\]_\(\)\-~])', re.DOTALL)

    def to_html(self, string):

        string = string or ''

        # вырезать все ссылки из документа, заменяя на специальный паттерн
        links = self.link.findall(string)
        string = self.link.sub('$$$LINK$$$', string)

        hints = self.hint.findall(string)
        string = self.hint.sub('$$$HINT$$$', string)

        string = self.strike.sub(r'<del>\1</del>', string)
        string = self.bold.sub(r'<strong>\1</strong>', string)
        string = self.italic.sub(r'<em>\1</em>', string)
        string = self.list.sub(r'<li>\1</li>', string)
        string = self.nl.sub(r'<br/>', string)

        # вернуть ссылки с форматированием текста
        for link in links:
            link_text, link_href = link
            if link_text == link_href:
                if len(link_text) > 40:
                    link_text = link_text[:30] + '...'
            else:
                link_text = self.to_html(link_text)
            # разэкранирование ссылок
            # TODO: можно случайно разэкранировать лишнее...
            link_href = link_href.replace('%28', '(').replace('%29', ')')
            string = string.replace('$$$LINK$$$', '<a href="%s" target="_blank">%s</a>' % (link_href, link_text), 1)

        # вернуть подсказки с форматированием текста
        for hint in hints:
            hint_ancor, hint_title = hint
            hint_title = self.to_html(hint_title)
            string = string.replace('$$$HINT$$$', '<abbr title="%s">%s</abbr>' % (hint_title, hint_ancor), 1)

        string = self.unescape.sub(r'\1', string)

        return string

    def to_text(self, string):

        string = string or ''

        # вырезать все ссылки из документа, заменяя на специальный паттерн
        links = self.link.findall(string)
        string = self.link.sub(r'\1', string)

        string = self.strike.sub(r'\1', string)
        string = self.bold.sub(r'\1', string)
        string = self.italic.sub(r'\1', string)
        string = self.list.sub(r'\1\n', string)

        string = self.unescape.sub(r'\1', string)

        return string


@register.filter(is_safe=True)
@stringfilter
def markdown2_filter(value):
    wv = WebView()
    return mark_safe(wv.to_html(force_text(value)))

register.filter('markdown', markdown2_filter)
