# coding: utf-8
#
import json
from django.template.loader import render_to_string
from django.conf import settings
import six


class DictatorContent(six.text_type):
    """
    Рендеринг блоков для разных задач
    """
    @property
    def html(self):
        """
        HTML по умолчанию (для всего)
        """
        html = []
        if len(self):
            try:
                content = json.loads(self)
                for block in content['data']:
                    html.append(self.block_to_html(block))
                    # или всё заканчивается на блоке cut
                    # if block['type'] == 'cut':
                    #     break
            except ValueError:
                pass
        return u''.join(html)
    
    @staticmethod
    def block_to_html(block):
        template_name = 'dictator/blocks/%s.html' % block['type']
        context = block['data']
        context['HTTP_HOST'] = settings.HOST
        return render_to_string(template_name, context)
    
    @property
    def json(self):
        """
        Просто JSON-объект
        """
        if len(self):
            return json.loads(self)

    @property
    def raw_indent(self):
        """
        Вывод в виде сериализованного в строку JSON без обработки
        """
        result = u''
        if len(self):
            try:
                result = json.dumps(json.loads(self), indent=4, ensure_ascii=False)
            except ValueError:
                result = '== err =='
        return result
