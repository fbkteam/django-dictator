# coding: utf-8
#
import json
from django import forms
from django.conf import settings
from django.utils.encoding import force_text


class DictatorWidget(forms.Textarea):
    """
    Виджет для модельного поля DictatorField.
    Работает как textarea, к которой цепляется код редактора.
    """

    def __init__(self, *args, **kwargs):
        if 'attrs' not in kwargs:
            kwargs['attrs'] = {}
        class_list = kwargs['attrs'].get('class', '').split()
        class_list.append('js-dictator-instance')
        kwargs['attrs']['class'] = ' '.join(class_list)

        # Параметры, которые передаются в виджет через data-атрибуты тега textarea
        dictator_conf = {
            'blocks': kwargs.pop('dictator_block_types', settings.DICTATOR_BLOCK_TYPES),
            'attachments': str(kwargs.pop('dictator_upload_url', force_text(settings.DICTATOR_UPLOAD_URL))),
            'focus': None,
        }
        kwargs['attrs']['data-dictator-conf'] = json.dumps(dictator_conf)

        super(DictatorWidget, self).__init__(*args, **kwargs)

    class Media:
        js = [
            'dictator/editor.js',
            'dictator/init.js',
        ]
        css = {
            'all': [
                'dictator/editor.css',
                'dictator/editor_to_site.css',
                'dictator/dictator.css'
            ]
        }
