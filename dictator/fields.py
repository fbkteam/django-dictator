from django.db.models import Field
from django.utils.six import with_metaclass, text_type
from django.utils.translation import ugettext_lazy as _
from . import DictatorContent
from .forms import DictatorFormField


# class SirTrevorField(with_metaclass(models.SubfieldBase, models.Field)):
class DictatorField(Field):
    description = _("TODO")

    def __init__(self, *args, **kwargs):
        self.dictator_conf = kwargs.pop('dictator_conf', {})
        super(DictatorField, self).__init__(*args, **kwargs)

    def get_internal_type(self):
        return 'TextField'

    def formfield(self, **kwargs):
        defaults = {
            'form_class': DictatorFormField,
            'dictator_conf': self.dictator_conf
        }
        defaults.update(kwargs)
        return super(DictatorField, self).formfield(**defaults)

    def from_db_value(self, value, expression, connection, context):
        return DictatorContent(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        return text_type(value)
