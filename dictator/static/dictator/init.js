var $ = django.jQuery;

$(document).ready(function(){

  $('textarea.js-dictator-instance').each(function(i, el) {

    var $el = $(el);

    if ($el.data('dictator')) return;

    var $container = $('<div class="dictator-container"></div>').insertBefore($el);

    var conf = $el.data('dictator-conf');
    var data = JSON.parse($el.val() || '{"data": []}')['data'];

    $el.hide();
    $el.parent().find('label').hide();

    console.log(JSON.stringify(data, null, 4 ));

    var editor_instance = createEditor({
      container: $container,
      blocks: conf['blocks'],
      data: data,
      urls: {
        attachments: conf['attachments']
      }
    });

    $el.data('editor', editor_instance);
    $el.data('dictator', true);

    $('#post_form').on('submit', function(){
      var data = {
        data: editor_instance.getData()
      };
      $el.val(JSON.stringify(data, undefined, 4 ));
    });

  });

});
