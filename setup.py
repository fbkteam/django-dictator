from setuptools import setup

setup(
    name='django-dictator',
    version='0.1.0.dev1',
    description='Dictator for Django',
#    install_requires=['django'],
    packages=['dictator'],
    url='https://bitbucket.org/fbkteam/django-dictator',
    zip_safe=False,
    include_package_data=True,
)
